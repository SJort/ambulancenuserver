<html>
   <head>
      <title>nicotin</title>
   </head>

   <body>
<?php
$dsn = 'mysql:dbname=mydb;host=localhost;port=3306;charset=utf8';
$connection = new \PDO($dsn, "root", "root");

// throw exceptions, when SQL error is caused
$connection->setAttribute(\PDO::ATTR_ERRMODE, \PDO::ERRMODE_EXCEPTION);
// prevent emulation of prepared statements
$connection->setAttribute(\PDO::ATTR_EMULATE_PREPARES, false);

if($_GET["request"] == "getHospitals"){
	$statement = $connection->prepare("SELECT * FROM Ziekenhuis");
	$statement->execute();
	$results = $statement->fetchAll(PDO::FETCH_ASSOC);
	$json = json_encode($results);

	echo $json;
}

if($_GET["request"] == "retrieveMessages"){

	if (filter_var($_GET["ticket_id"], FILTER_VALIDATE_INT) and $_GET["ticket_id"] >= 0){	
		$statement = $connection->prepare("SELECT * FROM Message where Ticket_id = :parameter");
		$statement->bindParam(':parameter',$_GET["ticket_id"],PDO::PARAM_STR);
		$statement->execute();
		$results = $statement->fetchAll(PDO::FETCH_ASSOC);
		$json = json_encode($results);
		echo $json;
	}else{
		echo 'invalid ticket_id';
	}
}

if($_GET["request"] == "retrieveTickets"){

	$statement = $connection->prepare("SELECT * FROM Ticket");
	$statement->execute();
	$results = $statement->fetchAll(PDO::FETCH_ASSOC);
	$json = json_encode($results);
	echo $json;
}

if($_GET["request"] == "sendMessage"){
	$corvalue = true;

	if (!filter_var($_GET["ticket_id"], FILTER_VALIDATE_INT) and $_GET["ticket_id"] >= 0){
		$corvalue = false;
		echo 'invalid user_id <br>';
	}
	if (!filter_var($_GET["user_id"], FILTER_VALIDATE_INT) and $_GET["user_id"] >= 0){
		$corvalue = false;
		echo 'invalid ticket_id <br>';
	}

	if($corvalue){
		$statement = $connection->prepare("insert into Message values('0',NOW(),'',:userid,:ticketid,:msg)");

		$statement->bindParam(':userid',$_GET["user_id"],PDO::PARAM_INT);
		$statement->bindParam(':ticketid',$_GET["ticket_id"],PDO::PARAM_INT);
		$statement->bindParam(':msg',$_GET["message"],PDO::PARAM_STR);
		$statement->execute();
		echo 'success <br>';
	}
}

if($_GET["request"] == "sendTicket"){

	$statement = $connection->prepare("insert into Ticket values('0',NOW(),:name,:bloodtype,:gender,:situation,:misc)");

	$statement->bindParam(':name',$_GET["name"],PDO::PARAM_STR);
	$statement->bindParam(':bloodtype',$_GET["bloodtype"],PDO::PARAM_STR);
	$statement->bindParam(':gender',$_GET["gender"],PDO::PARAM_STR);
	$statement->bindParam(':situation',$_GET["situation"],PDO::PARAM_STR);
	$statement->bindParam(':misc',$_GET["misc"],PDO::PARAM_STR);
	$statement->execute();
	$statement = $connection->prepare("select last_insert_id()");
	$statement->execute();
	$result = $statement->fetchAll(PDO::FETCH_ASSOC);
	echo 'success, ticket_id = ';
	echo $result[0]["last_insert_id()"];
	echo "<br>";
}

if(isset($_POST['submit'])){
	$corvalue = true;
	if (!filter_var($_POST["ticketid"], FILTER_VALIDATE_INT) and $_POST["ticketid"] >= 0){
		$corvalue = false;
		echo 'invalid user_id <br>';
	}
	if (!filter_var($_POST["userid"], FILTER_VALIDATE_INT) and $_POST["userid"] >= 0){
		$corvalue = false;
		echo 'invalid ticket_id <br>';
	}

	if($corvalue){

		if(!$_FILES['file']['size']==0){
			$file = $_FILES['file'];

			$fileName = $_FILES['file']['name'];
			$fileTmpName = $_FILES['file']['tmp_name'];
			$fileSize = $_FILES['file']['size'];
			$fileError = $_FILES['file']['error'];
			$fileType = $_FILES['file']['type'];

			//get file extension, put that in a variable and
			//make it all lower case
			$fileExt = explode('.',$fileName);
			$fileActualExt = strtolower(end($fileExt));

			$allowed = array('jpg','jpeg','png');
			if(in_array($fileActualExt, $allowed)) {
				if($fileError === 0) {
					if($fileSize < 50000000) {
						//make new unique filename based on current time in
						//millis and then append the lowercase fileextension
						//to it
						$fileNameNew= uniqid('',true).".".$fileActualExt;
						$filePath = 'pics/'.$_POST["ticketid"];
						$fileDestination = $filePath.'/'.$fileNameNew;

						if(file_exists($filePath) or mkdir($filePath,0777)){
							if(move_uploaded_file($fileTmpName,$fileDestination)){
								echo 'file uploaded <br>';
							} else {echo 'failed to write file <br>';}
						} else {echo 'failed to create folder <br>';}

					} else {echo 'filesize too large <br>';}
				} else {echo 'upload error <br>';}
			} else {echo 'wrong filetype <br>';}
		} else {echo 'no file found <br>';}

		$statement = $connection->prepare("insert into Message values('0',NOW(),:path,:userid,:ticketid,:msg)");

		$statement->bindParam(':path',$fileDestination,PDO::PARAM_STR);
		$statement->bindParam(':userid',$_POST["userid"],PDO::PARAM_INT);
		$statement->bindParam(':ticketid',$_POST["ticketid"],PDO::PARAM_INT);
		$statement->bindParam(':msg',$_POST["messagetxt"],PDO::PARAM_STR);
		$statement->execute();
		echo 'success <br>';
	} else {echo 'failure <br>';}
}

?>
   </body>
</html>





